# Revisió final del GRUB

Aquesta documentació es pot consultar en les pàgines
<https://gitlab.com/jordinas/fed-at-inf/tree/master/F24> o
<https://github.com/fadado/fed-at-inf/blob/master/F24>.

**Aquestes operacions les han de realitzar els alumnes del matí.**

1. Configurem el GRUB correctament per a l’arrancada dual.

    Verifiquem que el sistema s’ha iniciat amb la partició del mati (`/dev/sda5`).

        mount | grep '/dev/sda5 on /'

    Si no hi ha sortida tornem a arrencar i ara triem correctament la partició.
    Continuem on erem:

    ```
    # cp /boot/grub2/grub.cfg /boot/grub2/grub.backup
    # grub2-install /dev/sda
    # grub2-mkconfig -o /boot/grub2/grub.cfg
    ```

    Ara cal editar `/boot/grub2/grub.cfg` i modificar les entrades de les
    particions **normals**, no les de _rescue_ o _advanced_. El que hem de
    verificar dins del fitxer `/boot/grub2/grub.cfg` és el _timeout_ i el text
    de les etiquetes. Ha de quedar així (usant sempre caràcters ASCII, i mai accents, etc.):

    ```
    ...
    set timeout=-1
    ...
    menuentry 'Fedora MATI' ...
    ...
    menuentry 'Fedora TARDA' ...
    ```

    Els més valents podem esborrar també `rhgb quiet`.

    Després de fer els canvis i sortir de l'editor podem "xequejar"
    si hi hem comés algun error:
    ```
    grub2-script-check /boot/grub2/grub.cfg
    ```
    Com sempre *no news, good news*

<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->
