# Configuració en el servidor

Aquest fitxer documenta la configuració en el servidor que fa possible la instal·lació en les aules.
Per simplement instal·lar Fedora en les aules no cal estudiar aquesta pàgina.

## Imatges

Per qui les pugui necessitar aquestes imatges estan disponibles en el directori
`/home/groups/inf/public/install/ISO`:

* `Fedora-Workstation-Live-x86_64-27-1.6.iso`
* `Fedora-Workstation-netinst-x86_64-27-1.6.iso`

### Imatge instal·lable de Fedora

En el directori `/var/ftp/pub/images` s’ha copiat la imatge `Fedora-Workstation-netinst-x86_64-27-1.6.iso`.

En lloc de descomprimir la imatge la muntem. Preparem directori on muntar:

    $ sudo mkdir /var/ftp/pub/Fedora-Workstation-netinst-x86_64-27/

Configurem `/etc/fstab` muntant en el _loop_ la imatge:

    $ sudo cat <<'EOF' >> /etc/fstab
    /home/ftp/pub/images/Fedora-Workstation-netinst-x86_64-27-1.6.iso /var/ftp/pub/Fedora-Workstation-netinst-x86_64-27/  iso9660 loop 0 0
    $ sudo mount -a

Atenció: `/var/ftp/pub` és un enllaç a `/home/ftp/pub`.

## PXE

### Configurar DHCP

En el fitxer `/etc/dhcp/dhcpd.conf` hi ha unes línies màgiques relacionades amb PXE:

    option space PXE;
    option PXE.mtftp-ip    code 1 = ip-address;
    option PXE.mtftp-cport code 2 = unsigned integer 16; 
    option PXE.mtftp-sport code 3 = unsigned integer 16; 
    option PXE.mtftp-tmout code 4 = unsigned integer 8;
    option PXE.mtftp-delay code 5 = unsigned integer 8;
    option arch code 93 = unsigned integer 16; # RFC4578

Dins la configuració general de la subxarxa, hi ha aquestes paràmetres, dels
quals els dos darrers específics de PXE i TFTP:

    subnet 192.168.0.0 netmask 255.255.0.0 {
        # default gateway
            option routers 192.168.0.1;
            option subnet-mask 255.255.0.0;
        # DNS
            option domain-name "informatica.escoladeltreball.org";
            option domain-name-servers 192.168.0.10, 10.1.1.200, 193.152.63.197;
        # TFTP server i boot file (ruta relativa a /var/lib/tftpboot/)
            next-server 192.168.0.10;
            filename "fedora-install/pxelinux.0";
        ...

Després, per cada host amb MAC registrada hi ha una entrada com aquesta:

    host a34 {
        hardware ethernet 74:d4:35:ce:51:f4;
        fixed-address 192.168.4.34;
        option host-name "a34";
    }

### Configurar TFTP

Els clients descobreixen quin és el servidor de `tftpd` amb el paràmetre
`next-server` definit a `/etc/dhcp/dhcpd.conf`.

Per facilitar la gestió de dades els directoris propis d'aquest servei són físicament
a `/home`, però enllaçats en les seves rutes tradicionals:

    $ rmdir /var/lib/tftpboot
    $ ln -fs /home/var-lib-tftpboot /var/lib/tftpboot

El servidor `tftpd` arrenca per `xinetd` o similar amb el paràmetre `-s
/var/lib/tftpboot`. Això fa un `chroot` a aquest directori. Si cal instal·lem:

    $ sudo yum install tftp-server xinetd

Per defecte en la configuració de `xinetd` cal permetre el funcionament de `tftpd`
fent que el fitxer `/etc/xinetd.d/tftpd` tingui l’opció `disable = no`.

El client PXE demana la imatge d’arrencada `fedora-install/pxelinux.0`, que
està configurada en el seu comportament en base a les configuracions situades
en el directori `fedora-install/pxelinux.cfg`, on concretament el fitxer
`default` conté la descripció del menú principal conté la descripció del menú
principal.

El fitxer `pxelinux.0` cal obtenir-lo entre els proporcionats en el paquet `syslinux`:

    $ rpm -ql syslinux | grep pxelinux.0
    /usr/share/syslinux/gpxelinux.0
    /usr/share/syslinux/pxelinux.0

### Configurar una imatge

Cal crear directori per `tftp`:

    $ sudo mkdir /var/lib/tftpboot/fedora-install/installf24

Dins la imatge muntada hi ha fitxers a posar dins del directori del `tftp`:

    $ cd /var/ftp/pub/Fedora-24-wn-x86_64/images/pxeboot
    $ sudo cp initrd.img vmlinuz /var/lib/tftpboot/fedora-install/installf24

En el fitxer `/var/lib/tftpboot/fedora-install/pxelinux.cfg/default` cal afegir
l’entrada per el darrer Fedora:

    $ cd /var/lib/tftpboot/fedora-install/pxelinux.cfg
    $ vim default ...

    # ----------------------------------------------------------------------------
    # Instal·lacions Fedora 24 Workstation netinstall
    # ----------------------------------------------------------------------------
    LABEL Fedora24-wn-x86_64
        MENU LABEL Fedora 24 x86_64 (instal.lacio fedora 24 workstation netinstall)
        KERNEL installf24/vmlinuz
        APPEND initrd=installf24/initrd.img repo=ftp://gandhi/pub/Fedora-24-wn-x86_64/
        TEXT HELP
        Instal.lacio del Fedora 24 Arquitectura x86_64 (64 bits) workstation netinstall
        ENDTEXT
        
    MENU SEPARATOR

Atenció: en aquest fitxer `default` hi ha _hardcoded_ rutes amb el protocol FTP.

## FTP

Per facilitar la gestió de dades els directoris propis d'aquest servei són físicament
a `/home`, però enllaçats en les seves rutes tradicionals:

    $ rmdir /var/ftp/pub
    $ ln -fs /home/ftp/pub /var/ftp/pub

En la configuració de PXE hi ha _hardcoded_ rutes de l’estil `ftp://ftp...`, pel que
cal per tant disposar del servei FTP amb el nom definit per DNS. Si cal fem:

    $ sudo yum install vsftpd

En alguna fase de la instal·lació cal accedir a fitxers situats en el servidor.
Aquests es descarreguen amb el protocol FTP amb usuari anònim. El servidor `vsftpd` no
requereix, fora de permetre l&rsquo;accés públic,  de cap configuració especial
per oferir els fitxers situats a `/var/ftp/pub`. Per exemple, podem descarregar
fitxers amb ordres com ara `curl` o `wget`:

    $ curl ftp://ftp/pub/deploy.sh
    $ wget ftp://ftp/pub/deploy.sh

El que sí cal saber és que el directori _arrel_ del servidor FTP es configura a `/etc/passwd`, de manera que el directori
*home* de l’usuari **ftp** (`/home/ftp`) ès d’on ha de penjar `pub`. A més a més, el servidor fa **chroot** a aquest directori, pel que no es poden
tenir dins d’ell links simbòlics que surtin a fora d&rsquo;aquest arbre de directoris.

## DNS

Els fitxers relacionats amb la resolució de noms són, els tradicionals

* `/etc/hosts` (contingut d’exemple):
    ```
    127.0.0.1 localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1 localhost localhost.localdomain localhost6 localhost6.localdomain6
    192.168.0.10 gandi.informatica.escoladeltreball.org gandi
    192.168.0.5 russell.informatica.escoladeltreball.org russell
    ```

* `/etc/resolv.conf` (contingut d’exemple):
    ```
    search informatica.escoladeltreball.org
    nameserver 192.168.0.10
    ```

i els propis de **bind** (no copiats aquí):

* `/etc/named.conf`
* `/var/named/informatica.escoladeltreball.org.zone`
* `/var/named/informatica.escoladeltreball.org.rev.zone`
* `/etc/named.rfc1912.zones`

Aquests fitxer, no cal dir-ho, són de vital importància, i cal conservar-los
acuradament i probablement documentar millor el seu contingut en forma de comentaris.

<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->
