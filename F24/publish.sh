#!/bin/sh

DESTINATION='/tmp/F24'

[[ -e $DESTINATION ]] || mkdir $DESTINATION

for md in *.md
do
	html=${md%.md}.html

	sed -e 's/\.md)/.html)/g' "$md" \
	| pandoc \
		--from markdown --to html \
		--standalone \
		--include-in-header=style.htm \
		--output "$DESTINATION/$html" \
		-
done

cd $DESTINATION
{
cat <<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta name="generator" content="pandoc" />
  <title>Fed@INF</title>
  <style type="text/css"><!--
  body {
  	font:14px helvetica,arial,freesans,clean,sans-serif;
  	color:black;
  	line-height:1.4em;
  	background-color: #F8F8F8;
  }
  a:link {
  	text-decoration: none;
  }
  --></style>
</head>
<body>
<h1>Documents</h1>
<ul>
EOF

for f in *.html
do
	[[ $f == 'index.html' ]] && continue
	echo "<li><a href="$f">$f</a></li>"
done

cat <<EOF
</ul>
</body>
</html>
EOF
} > index.html

# vim:ts=4:sw=4:ai:syntax=sh
