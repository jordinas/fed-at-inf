# Instal·lació de Linux Fedora-24

**Curs 2017-2018**

Els enllaços a aquesta documentació i els seus repositoris es poden consultar en la pàgina
<http://tinyurl.com/fed-at-inf/documents>.

## Arrencada

Instal·larem el sistema operatiu Fedora per xarxa. Per fer-ho ens calen
targetes de xarxa compatibles amb el protocol PXE. Segons els ordinadors el
procés d’arrancada de la instal·lació pot ser:

* **Aula N2J**: Els ordinadors fan boot de la xarxa utilitzant el protocol PXE. En
  la arrancada cal prémer F12 o F8 segons els ordinadors.
* **Aula N2I**: Els ordinadors fan boot de la xarxa utilitzant el protocol PXE. En
  la arrancada cal prémer F12, F8, F2&hellip;
* **Aula N2H**: Els ordinadors fan boot de la xarxa utilitzant el protocol PXE. En
  la arrancada cal prémer F12.

Apareix un menú. Hem de seleccionar la darrera versió de Fedora.

<!--
_Ull! tenim el teclat anglès, de manera que podeu tenir problemes en intentar
escriure els caràcters `–`  i `_`:

* guió (`-`): useu la tecla del teclat numèric, no la del teclat alfabètic
* sotsguió (`_`): com si volguéssiu escriure `?`
-->

## Instal·lació i configuració

Un cop descarregada la imatge apareix la pantalla per triar l’idioma.
**Important**: triem com a idioma d’instal·lació l’**English (United States)**.

Ara cal configurar unes quantes coses que apareixen
com a icones a la pantalla. En acabar cada una d’elles cal completar la configuració
prement el botó **Done** que serà a la part superior esquerra.

No hem de modificar **NETWORK & HOST NAME**.

1. _KEYBOARD_:
    * usem el botó + per afegir català/valencià
    * usem el botó – per eliminar l’anglès (estem eliminant el teclat, no la
      llengua)

    Al requadre de la dreta podrem comprovar que les tecles funcionin
    correctament (prova `Ñ`, `ç`, etc.).

2. _LANGUAGE SUPPORT_: Marquem _Español (España)_ i _Català (Espanya)_ a més
   de conservar l’anglès.

3. _TIME & DATE_: Comprovem que estigui seleccionat _Madrid_ (o _Andorra_ ;-)

4. _INSTALLATION SOURCE_: Triem _On the network_, i en el desplegable mantenim
   **Closest mirror**.

    **Updates**: al checkbox no ha d’estar activat **don't install the latest available updates&hellip;**

5. _SOFTWARE SELECTION_: Ha d’estar seleccionat **Fedora Workstation** al requadre de l’esquerra.
    Al requadre de la dreta cal marcar:
    * **Administration Tools**
    * **Editors**
    * **Libre Office**
    
6. _SYSTEM_, _INSTALLATION DESTINATION_ (partició del disc dur):
    **Important: tan sols alumnes matí**! La resta d&rsquo;alumnes ha de passar 
    al punt següent.

    Ara deixarem la pantalla gràfica per anar a una consola; en acabar aquest
    punt tornarem a aquesta pantalla gràfica.

    Cal anar a una terminal de consola prement **Ctrl+Alt+F2**. Això mostrarà
    el prompt (es poden mirar altres consoles interessants també: **Alt-F3**,
    **Alt-F4**, **Alt-F5**, etc.):

        [anaconda rootpcxx /]#

    (que ens diu que som _root_).

    Per configurar el teclat correctament escrivim l’ordre `loadkeys es` que
    indica que és un teclat espanyol.  Executem el programa `fdisk /dev/sda`
    per particionar el disc dur.  La tecla `m` és l’ajuda del programa
    `fdisk`.

    Eliminem totes les particions.

    Afegim les particions següents:

    1. Una partició estesa (extended) **sda1** que ocupa **+205G** (100 + 100
       +5 GBytes).
    1. Dins de l’estesa una partició lògica **sda5** de **+100G** (100 Gbytes)
       pel Linux.
    1. Dins de l’estesa una partició lògica **sda6** de **+100G** (100 Gbytes)
       pel Linux.
    1. També dins de l’estesa una partició lògica **sda7** amb la resta d’espai
       de l’estesa pel swap. Declarem la partició de swap com a tipus 82, amb
       la comanda `t` de fdisk.

    Desem els canvis fets amb `fdisk` (és a dir la tecla `w` de write). Això
    reescriurà la taula de particions del disc.

    Tornem a l’entorn gràfic prement **Ctrl+Alt+F6** (o similar).

7. _SYSTEM_, _INSTALLATION DESTINATION_ (organització de les particions):
   * Seleccionem **INSTALLATION DESTINATION**.
   * A **Other Storage Options / Partitioning** seleccionem **I will configure
     partitioning** (farem un particionat manual).
   * Comprovem que el disc està seleccionat amb una marca de check (⎷).
   * Premem **Done**.
   * **Nomes al mati**: cliquem en la part inferior esquerra el
     botó **Reload storage configuration from disk**.
     Se’ns obre una finestra i hem de tornar a fer clic a **Rescan Disks**. Fem
     clic a **OK**, el que ens porta a la pantalla anterior; tornem a prémer
     **Done** per continuar.
   * Apareix a l’esquerra la finestra **MANUAL PARTITIONING**.
   * Al menú desplegable on es selecciona la **&hellip;partitioning scheme:** està
     seleccionat per defecte LVM, s’ha de canviar per **Standard Partition**.

8. _SYSTEM_, _INSTALLATION DESTINATION_ (selecció de la partició arrel):
    Escollim la partició de 100 GB corresponent (**sda5** al matí, **sda6** a
    la tarda) i l’editem:
    * Marquem **Reformat**.
    * Despleguem **File System** i triem **ext4**.
    * Posem **Mount Point** a l’arrel, o sigui `/`.
    * Posem el **Label** pertinent: _MATI_ o _TARDA_ segons correspongui,
      exactament com està indicat aquí, en majúscules.
    * Cliquem **Update Settings**.

9. _SYSTEM_, _INSTALLATION DESTINATION_ (selecció de la partició d’intercanvi):
    Escollim la partició d’intercanvi (_swap_) **sda7** i l'editem:
    * Marquem **Reformat**.
    * Despleguem **File System** i triar **swap**.
    * No necessita punt de muntatge.
    * Cliquem el botó **Update Settings**.
    * Acabem el particionat del disc prement **Done**.
    * En el quadre de diàleg cliquem **Accept Changes**.

10. Comencem la instal·lació:
    * Premem **Begin Installation**.
    * Entrem _el planeta del sistema solar indicat_ com a password de _root_ (tot en minúscules).
      En prémer
      **Done** es queixarà per _weak password_: premem **Done** un altre cop.
    * Creem un usuari local provisional de nom _guest_, amb la contrasenya
      que vulguis. Aquest usuari pot ser destruït una vegada completada la
      instal·lació, però ara ens serveix d’exemple de com podriem crear un
      usuari **no** administrador en el moment d’instal·lar Fedora.
      **Important**: la contrasenya et farà falta en la fase [Configuració](02-Configuracio.md)!
    * Esperem que la instal·lació estigui completa (cal que
      digui **Complete!**) i premem **Reboot**.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

**Final instal·lació bàsica**

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

Seguim amb les instruccions del fitxer sobre [Configuració](02-Configuracio.md):
caldrà iniciar sessió gràfica com l’usuari _guest_ i obrir el _Firefox_ per poder
consultar el fitxer [Configuració](02-Configuracio.md).

<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->

