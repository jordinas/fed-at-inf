﻿# Personalització de Linux Fedora-24

# Curs 2017-2018

Els enllaços a aquesta documentació i els seus repositoris es poden consultar en la pàgina
<http://tinyurl.com/fed-at-inf/documents>.

## Punts clau on posar atenció

* Quan posem la nostra contrasenya d’usuari hem de vigilar de no usar el teclat
  **numèric**. També cal verificar de no prèmer la tecla **Bloq Mayús**.

## Personalització

1. Iniciem sessió gràfica amb el nostre propi compte d’usuari.  Verifiquem en un
   terminal l’accés al compte d’usuari modificant la contrasenya. Cal que
   compleixi unes determinades normes de longitud i combinació de lletres i
   xifres. 
   
    ```
    $ passwd
    ```

2. Ens posem com a usuari _root_ (amb l’ordre `su -` ) i instal·lem el servei
   `gpm`; el configurarem per tenir-lo sempre disponible:

    **Nota**: sempre que es faci l’ordre `su` cal fer-la apropiadament: `su` espai i guió.

    ```
    $ su -
    # dnf -y install gpm
    # systemctl enable gpm.service
    # systemctl start gpm.service  
    ```

    Podem també assegurar el compte de convidat amb l’ordre:

    ```
    # dnf -y install pwgen
    # pwgen -1 | passwd --stdin guest
    ```

3.  Repositoris de tercers.

    RPM Fusion:

    ```
    # rpm -ivh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-24.noarch.rpm
    # rpm -ivh http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-24.noarch.rpm
    ```

    Adobe flash plugin per Firefox:

    ```
    # dnf -y install http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
    # rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-adobe-linux
    # dnf -y install flash-plugin
    ```

    Verifiquem la llista actual de repositoris:

    ```
    # dnf repolist
    ```

    Deixem de ser _root_ per tornar a l’usuari personal amb l’ordre:

    ```
    # exit
    ```

4. Espai en disc.

    Tots els usuaris disposem d'una quota de 100 MB al servidor de departament. Per saber l’espai ocupat i controlar possibles fitxers massa grans podem executar l'ordre:
    ```
    $ du -h --max-depth=1 $HOME/$USERNAME 
    ```

5. Des de la sessió grafica de l’usuari **Desactivar el So** de les alertes de  teclat
    * Buscar l’aplicació de GNOME _Sound_
    * Clicar sobre la pestanya **Sound Effects**
    * Marcar **Alert Volume OFF**

6. Per desactivar les actualitzacions
   automatiques del sistema en el nostre perfil d’usuari cal executar:

        $ gsettings set org.gnome.software download-updates false


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

**Final personalització**

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

## Procediments de verificació

* Funcionament correcte de Kerberos: simplement iniciem sessió en una
  consola  amb el compte personal d’usuari (anem a la consola prement **Ctrl+Alt+F3**).
    
* Els directoris del servidor estan correctament exportats?

        $ mount | grep gandhi

* Verificació dels repositoris externs:

        $ dnf repolist

* Verificació de l’estat del _selinux_:

        $ getenforce
    
<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->
