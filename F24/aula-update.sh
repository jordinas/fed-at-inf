#!/bin/bash
# Filename:     aula-update.sh
# Author:       Jonatan
# Date:         27/06/2016
# Version:      0.8
# License:      This is free software, licensed under the GNU General Public License v3.
#               See http://www.gnu.org/licenses/gpl.html for more information."
# Usage:        ./aula-update.sh
# Description:  Script post-actualització paquets de Fedora aula alumnes.
#               Es crea un log de missatges i errors.


# Seguretat
set -o errexit -o pipefail -o nounset

# Desactivades algunes expansions 
IFS=''
set -o noglob

# Configurem serveis apropiadament:
function configure_services
{
    systemctl enable NetworkManager-wait-online.service
    systemctl stop firewalld.service
    systemctl disable firewalld.service
    systemctl disable sshd.service
}

# Instal·lem vi...:
function install_editors
{
    dnf -y install vim gvim  mc
}

# Cal desactivar selinux
function disable_selinux
{
    sed -i -e s,'SELINUX=enforcing','SELINUX=permissive', /etc/selinux/config
}

# gpm:


# Configuració amb el kerberos:
function configure_kerberos
{
    dnf -y install  krb5-devel krb5-workstation pam_mount
    #authconfig-tui
    authconfig  --enableshadow --enablelocauthorize --enableldap \
                --ldapserver='ldap' --ldapbase='dc=escoladeltreball,dc=org' \
                --enablekrb5 --krb5kdc='kerberos' \
                --krb5adminserver='kerberos' --krb5realm='INFORMATICA.ESCOLADELTREBALL.ORG' \
                --updateall
    cd /tmp
    curl ftp://ftp/pub/deploy.sh | bash
    systemctl enable nfs-secure.service
    systemctl restart nfs-secure.service
}

# Muntar el sistema de fitxers 
function mount_groups
{
    echo "gandhi:/groups/ /home/groups  nfs4  sec=krb5,comment=systemd.automount  0  0" >> /etc/fstab
    mount -a
}

# Excloure les actualitzacions del kernel
function exclude_updates
{
    echo 'exclude=kernel-4*,kernel-core,kernel-modules' >> /etc/dnf/dnf.conf
}

function install_gpm
{
    dnf -y install gpm
    systemctl enable gpm.service
    systemctl start gpm.service
}

# Configurar l'arrancada al mode multi-user.target
function configure_boot
{
    rm -f /etc/systemd/system/default.target
    ln -s /lib/systemd/system/multi-user.target /etc/systemd/system/default.target
}

# repositoris:
function add_repositories
{
    rpm -ivh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-24.noarch.rpm
    rpm -ivh http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-24.noarch.rpm
    dnf -y install http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
    rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-adobe-linux
    dnf -y install flash-plugin
}

# Actualitzar el sistema
function configure_gnome
{
    #Treiem les actualitzacions automàtiques
    gsettings set org.gnome.software download-updates false
    #dnf -y update
}

# Altres
function others
{
    dnf -y install vlc nmap wireshark-gnome ethtool @Virtualization
}

# Instal·ació de virt-manager i permetre'n l'ús pels usuaris normals
function configure_virt
{
    dnf -y install virt-manager libvirt-daemon-driver libvirt-daemon-config-network
    sed -i 's/#auth_unix_rw = "none"/auth_unix_rw = "none"/' /etc/libvirt/libvirtd.conf
    systemctl restart libvirtd.service
}

########################################################################
# Main
########################################################################

echo "---------------------------------------------------"
echo "POST Actualització paquets de Fedora aula alumnes"
echo "juny 2016 @edt"
echo "---------------------------------------------------"

LOGFILE=/tmp/$0-$$.log

{
	date -Iseconds

    configure_services
    install_editors
    disable_selinux
    configure_kerberos
    mount_groups
    exclude_updates
    install_gpm
    add_repositories
    configure_gnome

#    configure_boot
#    configure_virt
#    others

	date -Iseconds
} 2>&1 | tee $LOGFILE

exit

# vim:ts=4:sw=4:ai:et:syntax=sh
