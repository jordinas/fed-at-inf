# Enllaços

## Instal·lació de Fedora

* [Obtingueu Fedora](https://getfedora.org/)
* [Installation Quick Start Guide](https://docs.fedoraproject.org/en-US/Fedora/20/html/Installation_Quick_Start_Guide/index.html)
* [Installation Guide](http://docs.fedoraproject.org/en-US/Fedora/20/html/Installation_Guide/)
* [Preparing a USB flash drive as an installation source](http://docs.fedoraproject.org/en-US/Fedora/20/html/Installation_Guide/Making_USB_Media.html)
* També amb versió per Windows de [LiveUSB Creator](https://fedorahosted.org/liveusb-creator/)
* Actualització amb [FedUp](https://fedoraproject.org/wiki/FedUp)
* Paquets suplementaris amb [RPM Fusion](http://rpmfusion.org/Configuration)

## Post instal·lació

* [Fedy](http://folkswithhats.org/)
* [Personal Fedora 19 Installation Guide](http://www.mjmwired.net/resources/mjm-fedora-f19.html)
* [http://geekface.ca/fedora](http://geekface.ca/fedora/?q=installing)
* [18 things I did after installing Fedora 20, the Xfce spin](http://www.binarytides.com/better-fedora-20-xfce/)
* Més dubtes? Pregunta a [Ask Fedora](https://ask.fedoraproject.org/en/questions/)

## GNOME 3

* [Cheat Sheet](https://wiki.gnome.org/Projects/GnomeShell/CheatSheet)
* [GNOME Library](https://help.gnome.org/)

<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->
