#!/bin/sh

########################################################################
# Deployment script
# Cridat des del script d'instal·lació: copiat en aquest repo per comoditat!
########################################################################

# Descarreguem arxiu amb diversos fitxers de configuració
cd /tmp
rm -f config.tgz*

# Expandim fitxers de configuració
wget --quiet ftp://ftp.informatica.escoladeltreball.org/pub/config.tgz
tar --directory=/ --no-same-owner -x -v -z -f config.tgz

# Arreglem symlinks de pam.d
cd /etc/pam.d
ln -fs system-auth-ac system-auth
ln -fs password-auth-ac password-auth
cd /tmp

# Extranys permisos i usuari a /root
chown -R root.root /root
mkdir /root/.ssh &>/dev/null || true
chmod 750 /root /root/.ssh

# Permetem gestió remota del root dels profes als PCs dels alumnes
cat <<EOF >> /root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDmVo0PVXkSpaNlq9/s58ZqhmX6QLLgyCk7xUJ0AdkLOQcKbhpQdjQAY3XbG1qMg1areoIYrfsLuj6YY/mT8K/IlaDsBV+pzPjYDlh/tzBRFZxI+rgtYQB5gALa3rjcloKqLneejSgiDv1lgYGMhWWXTQk7dtV5pUcMYDaXY133/DX3YG3oZATsHec/sFGKVGIkdxkO/DksoGv5XbRC3b5BtFyAVvqF/vlxMQSHciNbG36PRdKESQ0rtV8mJsSbb3U+G11umU0/XXsiAdaYuu0OK65qOFJ7Av/TEC7Q5S3qxqzaQreRToPQGX5+ZdEREmwFKthmHPa/FHuDJqIQBY+N root@d02.informatica.escoladeltreball.org
EOF
chmod 640 /root/.ssh/authorized_keys

exit
