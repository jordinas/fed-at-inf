#!/bin/bash

# Script per automatitzar instal·lació.
# TAN SOLS per primer de GM!!!
# Es crea un log de missatges i errors.

# Seguretat
set -o errexit -o pipefail -o nounset

# Desactivades algunes expansions 
IFS=''
set -o noglob

# Configurem serveis apropiadament:
function configure_services
{
    systemctl enable NetworkManager-wait-online.service
    systemctl stop firewalld.service
    systemctl disable firewalld.service
    systemctl disable sshd.service
}

# Instal·lem vi...:
function install_editors
{
    dnf -y install vim-minimal vim gvim mc git tig
}

# Cal desactivar selinux
function disable_selinux
{
    sed -i -e 's/SELINUX=enforcing/SELINUX=permissive/' /etc/selinux/config
    setenforce 0
}

# gpm:
function install_gpm
{
    dnf -y install gpm
    systemctl enable gpm.service
    systemctl start gpm.service
}

# Configuració amb el kerberos:
function configure_kerberos
{
    dnf -y install krb5-devel krb5-workstation
    dnf -y install pam_mount # separat dnf en dos linies per JVinyoles
    #authconfig-tui
    authconfig  --enableshadow --enablelocauthorize --enableldap \
                --ldapserver='ldap' --ldapbase='dc=escoladeltreball,dc=org' \
                --enablekrb5 --krb5kdc='kerberos.informatica.escoladeltreball.org' \
                --krb5adminserver='kerberos.informatica.escoladeltreball.org' --krb5realm='INFORMATICA.ESCOLADELTREBALL.ORG' \
                --updateall
    cd /tmp
    curl ftp://ftp/pub/deploy.sh > /tmp/deploy.sh
    bash /tmp/deploy.sh
    systemctl enable nfs-secure.service
    systemctl restart nfs-secure.service
}

# Muntar el sistema de fitxers 
function mount_groups
{
    umount /home/groups >/dev/null 2>&1 || true
    sleep 1

# Opció 1: editar amb vim
#   echo 'Ara cal editar /etc/fstab. Prem Enter...'
#   read
#   vim /etc/fstab

# Opció 2: reescriu sencer fstab!
    # PER MATÍ!!!
    echo '/dev/sda5    /       ext4    defaults        1 1' > /etc/fstab
    # PER TARDA!!!
#   echo '/dev/sda6    /       ext4    defaults        1 1' > /etc/fstab
    echo '/dev/sda7  swap      swap    defaults        0 0' >> /etc/fstab
    echo 'gandhi:/groups/ /home/groups  nfs4  sec=krb5,comment=systemd.automount  0  0' >> /etc/fstab

# Al final esperar abans de muntar!
    sleep 2
    echo 'Mounting Ghandi...'
    mount -a
    echo 'Done'
}

# Excloure les actualitzacions del kernel
function exclude_updates
{
    echo 'exclude=kernel-4*,kernel-core,kernel-modules' >> /etc/dnf/dnf.conf
}

# Configurar l'arrancada al mode multi-user.target
function configure_boot
{
    rm -f /etc/systemd/system/default.target
    ln -s /lib/systemd/system/multi-user.target /etc/systemd/system/default.target
}

# repositoris:
function add_repositories
{
    dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
                   https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
    dnf -y install http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
    rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-adobe-linux
    dnf -y install flash-plugin
    dnf -y install chromium vlc gimp git tig gnome-tweak-tool
}

# Actualitzar el sistema
function configure_gnome
{
    #Treiem les actualitzacions automàtiques
    gsettings set org.gnome.software download-updates false
    #dnf -y update
}

# Altres
function others
{
    dnf -y install vlc nmap wireshark-gnome ethtool @Virtualization
}

# Instal·ació de virt-manager i permetre'n l'ús pels usuaris normals
function configure_virt
{
    dnf -y install virt-manager libvirt-daemon-driver libvirt-daemon-config-network
    sed -i 's/#auth_unix_rw = "none"/auth_unix_rw = "none"/' /etc/libvirt/libvirtd.conf
    systemctl restart libvirtd.service
}

function disable_guest
{
    dnf -y install pwgen
    pwgen -1 | passwd --stdin guest
}

function post_install
{
    echo 'Gandhi should be mounted:'
    mount | grep gandhi
    echo

    echo 'Selinux should be permissive:'
    grep permissive /etc/selinux/config 
    echo
    echo 'If everything is ok...'
    echo '   ...you can reboot now'
}

########################################################################
# Main
########################################################################

echo "---------------------------------------------------"
echo "Actualització paquets de Fedora aula alumnes"
echo "---------------------------------------------------"

LOGFILE=/tmp/$0-$$.log

{
    date -Iseconds
    for action in \
        configure_services \
        install_editors \
        disable_selinux \
        install_gpm \
        configure_kerberos \
        disable_guest \
        mount_groups \
        exclude_updates \
        add_repositories \
        configure_gnome \
        post_install
    do
        echo "==================================================="
        echo "ACTION: $action"
        echo "==================================================="
        $action
    done

# Accions opcionals o obsoletes...
#    configure_boot
#    configure_virt
#    others

    date -Iseconds
} 2>&1 | tee $LOGFILE

exit

# vim:ts=4:sw=4:ai:et:syntax=sh
