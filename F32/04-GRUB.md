# Revisió final del GRUB

Aquesta documentació es pot consultar en les pàgines
<https://gitlab.com/jordinas/fed-at-inf/tree/master/F32>.

**Aquestes operacions les han de realitzar els alumnes del matí.**

Ara configurarem el GRUB correctament per a l’arrancada dual.

1. Verificació de que el sistema s’ha iniciat amb la partició del matí (`/dev/sda5`).

    Executem:

    ```
    mount | grep '/dev/sda5 on /'
    ```

    Si no hi ha sortida tornem a iniciar el sistema i ara triem correctament la partició al GRUB.

    Assegurar-se de que aquesta vegada s'ha escollit l'entrada correcta tornant a executar l'ordre anterior.
    (Aquest seria un primer exemple de bucle)

2. Canviem alguns valors de configuració de GRUB:

    Podem editar el fitxer `/etc/default/grub`

    i canviem que el temps d'espera (timeout) que ens proporciona GRUB, de 5 segons a pausa (-1):

    ```
    GRUB_TIMEOUT=-1
    ```

    canviem a l'antic gestor d'arrencada: 

    ```
    GRUB_ENABLE_BLSCFG=false
    ```

    O alternativament podem fer:

    ```
    sed -i  -e s,'GRUB_TIMEOUT=5','GRUB_TIMEOUT=-1', -e  s,'GRUB_ENABLE_BLSCFG=true','GRUB_ENABLE_BLSCFG=false', /etc/default/grub
    ```

    Ara tornarem a instal·lar el grub a la nostra partició:

    ```
    grub2-install /dev/sda
    ```

    Fem una còpia de seguretat:

    ```
    cp /boot/grub2/grub.cfg /boot/grub2/grub.backup
    ```

    Detectem els sistemes operatiu i escrivim la configuració al fitxer adient:

    ```
    grub2-mkconfig -o /boot/grub2/grub.cfg
    ```
    
    Tornem a fer una còpia de seguretat:

    ```
    cp /boot/grub2/grub.cfg /boot/grub2/grub.backup2
    ```

    Ara cal editar `/boot/grub2/grub.cfg` i modificar les entrades de les
    particions **normals**, no les de _rescue_ o _advanced_. El que hem de
    verificar dins del fitxer `/boot/grub2/grub.cfg` és el text de les
    etiquetes. Ha de quedar així (usant sempre caràcters ASCII, i mai accents,
    etc.):

    ```
    ...
    menuentry 'Fedora MATI' ...
    ...
    menuentry 'Fedora TARDA' ...
    ```

    Recomanable, per detectar possibles errades a l'arrencar el sistema, esborrar `rhgb`.

    Després de fer els canvis i sortir de l'editor podem "xequejar"
    si hem comès algun error:
    ```
    grub2-script-check /boot/grub2/grub.cfg
    ```
    Com sempre *no news, good news*

    Una manera alternativa de modificar les línies del `/boot/grub2/grub.cfg` es fent servir l'eina gràfica grub-customizer:

    ```
    dnf install grub-customizer
    ```

    I executant-la després.


<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->
